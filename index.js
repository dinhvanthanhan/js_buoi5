function score() {
    var breakpoint = +document.getElementById("point").value;
    
    var first = +document.getElementById("firstPoint").value;
    var second = +document.getElementById("secondPoint").value;
    var third = +document.getElementById("thirdPoint").value;

    var area = +document.getElementById("area").value;
    var object = +document.getElementById("object").value;

    var myPoint = first + second + third + area + object;

    var pass = "Bạn đã đậu. Tổng điểm: " + myPoint;
    var fail = "Bạn đã rớt. Tổng điểm: " + myPoint;
    var unlucky = "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
    
    if(first == 0 || second == 0 || third == 0) {
        document.getElementById("notification1").innerHTML = `${unlucky}`;
    }else if(myPoint >= breakpoint) {
        document.getElementById("notification1").innerHTML = `${pass}`;
    } else {
        document.getElementById("notification1").innerHTML = `${fail}`;
    }
}

function electricity() {
    var name = document.getElementById("name").value;
    var kw = +document.getElementById("kw").value;
    var price;

    if(kw <= 50) {
        price = kw * 500;
    } else if(51 <= kw && kw <= 100) {
        price = 50 * 500 + (kw - 50) * 650;
    } else if(101 <= kw && kw <= 200) {
        price = 50 * 500 + 50 * 650 + (kw - 100) * 850;
    } else if(201 <= kw && kw <= 350) {
        price = 50 * 500 + 50 * 650 + 100 * 850 + (kw - 200) * 1100;
    } else {
        price = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kw - 350) * 1300;
    }

    document.getElementById("notification2").innerHTML = "Họ tên: " + name + ";Tiền điện: " + price;
    
}